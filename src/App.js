import * as React from "react";
import {
  createBrowserRouter,
  RouterProvider,
  Link,
} from "react-router-dom";
import LoginForm from "./components/LoginForm";
import CategoryList from "./components/CategoryList";

const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <div>
        <h1>Welcome to Monthly Budget!</h1>        
        <br />
        <Link to="categories">Categories</Link>
        <br />
        <Link to="login">Login</Link>
      </div>
    ),
  },
  {
    path: "/login",
    element: <LoginForm />,
  },
  {
    path: "categories",
    element: <CategoryList />,
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
